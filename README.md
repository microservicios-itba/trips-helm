# Trips

Trips Helm is a chart for Helm that allows you to quickly deploy in Kubernetes a micro-service architecture that supports the management of trips and airports. 

## Installation

You should have already installed:
- Kubernetes
- Helm

When setting up your Kubernets, you should create the folders for the volumes. If you are using minikubernetes, you can use:

```bash
$ minikube start --mount-string="HOST_PATH:/MINIKUBE_PATH" --mount
```

Now clone the repository, get into the folder and run Helm to deploy all resources.

```bash
$ git clone https://gitlab.com/microservicios-itba/trips-helm
$ cd trips-helm
$ helm install trips . --values values.yaml
```

## Config

If you want to change variables you can change the values in `values.yaml` or provide another file on install

#### Deployment Variables:
- `deployment.replicas.airports`: The number of replicas for the "airports" deployment. It is set to 2.
- `deployment.replicas.gateways`: The number of replicas for the "gateways" deployment. It is set to 2.
- `deployment.replicas.trips`: The number of replicas for the "trips" deployment. It is set to 2.

- `deployment.maxSurge.airports`: The maximum number of additional replicas allowed during a rolling update for the "airports" deployment. It is set to 25%.
- `deployment.maxSurge.gateways`: The maximum number of additional replicas allowed during a rolling update for the "gateways" deployment. It is set to 25%.
- `deployment.maxSurge.trips`: The maximum number of additional replicas allowed during a rolling update for the "trips" deployment. It is set to 25%.

- `deployment.maxUnavailable.airports`: The maximum number of unavailable replicas allowed during a rolling update for the "airports" deployment. It is set to 25%.
- `deployment.maxUnavailable.gateways`: The maximum number of unavailable replicas allowed during a rolling update for the "gateways" deployment. It is set to 0%.
- `deployment.maxUnavailable.trips`: The maximum number of unavailable replicas allowed during a rolling update for the "trips" deployment. It is set to 25%.

#### NodePorts Variable:
- `nodeports.gateway`: The NodePort number for the "gateway" service. It is set to 30000.

#### Volume Variables:
- `volumes.pv1.capacity`: The capacity of the "pv1" Persistent Volume (PV). It is set to 128Mi.
- `volumes.pv1.path`: The path where the "pv1" volume is mounted. It is set to "/volumes/pv1".
- `volumes.pv2.capacity`: The capacity of the "pv2" Persistent Volume (PV). It is set to 128Mi.
- `volumes.pv2.path`: The path where the "pv2" volume is mounted. It is set to "/volumes/pv2".

#### Persistent Volume Claim Variables:
- `claims.trips.redis`: The capacity of the "redis-trips" Persistent Volume Claim (PVC). It is set to 64Mi.
- `claims.airports.redis`: The capacity of the "redis-airports" Persistent Volume Claim (PVC). It is set to 64Mi.

## Usage

First you should get the IP of your Kubernetes cluster, if you are using minikubes, by default is `192.168.49.2`

For creating a trip

```bash
$ curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"airport_from":"EZE","airport_to":"CPC"}' \
  http://CLUSTER_IP:NODE_PORT/trip
```

For retrieving a trip

```bash
$ curl http://CLUSTER_IP:NODE_PORT/trip/<trip_id>
```

For creating an airport

```bash
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"airport": "Ezeiza"}' \
  http://CLUSTER_IP:NODE_PORT/airport
```

For retrieving an airpot

```bash
curl http://CLUSTER_IP:NODE_PORT/airport/<AIRPORT_ID>
```
